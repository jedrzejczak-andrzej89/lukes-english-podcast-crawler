var cheerio = require('cheerio');
var request = require('request');
var fs = require('fs');
var ProgressBar = require('progress');

function downloadFile(url, filename) {
    var request = require('request');
    var fs = require('fs');
    fs.exists(filename, function (exists) {
        if (exists && getFilesizeInBytes(filename) > 1000000) {
            console.log("File exists: " + filename + ' downloaded from ' + url);
        } else {
            var req = request(url);
            var bar = null;

            req.on('response', function (res) {
                    var len = parseInt(res.headers['content-length'], 10);
                console.log();
                    bar = new ProgressBar('Downloading ' + filename + '\t\t [:bar] :percent :etas \t\t' + 'from ' + url , {
                        complete: '=',
                        incomplete: ' ',
                        width: 20,
                        total: len
                    });
                });
                req.on('data', function (chunk) {
                    bar.tick(chunk.length);
                });
                req.on('start', function () {
                    //console.log("Downloading: " + url);
                });
                req.on('end', function () {
                    console.log('\n');
                    //console.log("Done: " + url + " saved as " + filename);
                });
                req.pipe(fs.createWriteStream(filename));
        }
    });
}

function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}

//downloadFile('http://www.tonycuffe.com/mp3/saewill.mp3', 'ss.mp3');
downloadFile('https://audioboo.fm/boos/2241366-183-luke-s-d-day-diary-part-1.mp3', '2.mp3');
downloadFile('https://audioboom.com/boos/3327237-282-questions-from-tea4er-ru.mp3 ', '3.mp3');

