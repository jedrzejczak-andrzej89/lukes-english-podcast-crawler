var cheerio = require('cheerio');
var request = require('request');
var fs = require('fs');

var podcastsDirectory = 'podcasts';

if(!fs.existsSync(podcastsDirectory)){
    fs.mkdirSync(podcastsDirectory, 0766, function(err){
        if(err){
            console.log(err);
        }
    });
}

request('http://teacherluke.co.uk/the-podcast/archive-of-episodes-1-149/', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        $ = cheerio.load(body);
        $detailsLinks = $('.entry-content p').find('a')
        $detailsLinks.each(function (i, item) {
            request($(item).attr('href'), function (error1, response1, body1) {
                if (!error1 && response1.statusCode == 200) {
                    var fileName = slugify(response1.request.uri.path) + '.mp3';
                    //process.exit(1);
                    $1 = cheerio.load(body1);
                    //title = $1('h1.entry-title').text();
                    //console.log(title, $(item).attr('href').replace('http://teacherluke.co.uk/', ''));
                    //process.exit(1);
                    $allSiteLinks = $1("a");

                    $allSiteLinks.each(function (i1, item1) {
                        if ($1(item1).text() == 'DOWNLOAD' || $1(item1).text() == 'Download Episode' || $1(item1).text() == 'Right-click here to download this episode.') {
                            //console.log('podcast link: ' + title);
                            downloadFile($1(item1).attr('href'), podcastsDirectory + '/' + fileName);

                        }
                    });
                }
            });
        });
    }
});

function downloadFile(url, filename) {
    var request = require('request');
    var fs = require('fs');

    fs.exists(filename, function (exists) {
        if (exists && getFilesizeInBytes(filename) > 1000000) {
            console.log("File exists: " + filename + ' downloaded from ' + url);
        } else {
            request(url)
                .on('start', function () {
                    console.log("Downloading: " + url);
                })
                .on('end', function () {
                    console.log("Done: " + url + " saved as " + filename);
                })
                .pipe(fs.createWriteStream(filename));
        }
    });

}

function slugify(text)
{
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '-')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}

function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}
